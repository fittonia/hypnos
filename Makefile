LINUX_DIR= ../linux-5.8

all: copy _hypnos gen-patch remove-tmp

copy:
	echo "Copying linux/security"
	mkdir -p linux_tmp1/security
	cp -R $(LINUX_DIR)/security/Kconfig linux_tmp1/security/Kconfig
	cp -R $(LINUX_DIR)/security/Makefile linux_tmp1/security/Makefile
	cp -R linux_tmp1 linux_tmp2

_hypnos:
	sed -i '/source "security\/yama\/Kconfig"/asource "security\/hypnos\/Kconfig"' linux_tmp1/security/Kconfig
	echo '' >> linux_tmp1/security/Makefile
	echo '# Hypnos LSM' >> linux_tmp1/security/Makefile
	echo 'obj-$$(CONFIG_SECURITY_HYPNOS)			+= hypnos/' >> linux_tmp1/security/Makefile
	echo 'subdir-$$(CONFIG_SECURITY_HYPNOS)		+= hypnos' >> linux_tmp1/security/Makefile
	cp -R hypnos linux_tmp1/security

gen-patch:
	git -C linux_tmp1 diff --output=../patch --no-index ../linux_tmp2/security security || true
	sed -i 's/\.\.\/linux_tmp2\///' patch

remove-tmp:
	rm -rf linux_tmp1 linux_tmp2