![hypnos.png](hypnos.png)

**Hypnos** intends to protect systems from applications which behave badly.

Processes use the machine's hardware by the means of system calls.
During a system call, the calling process gets paused and the operating system takes over.
Before doing what the process has requested, the OS decides whether or not it should do so.
In the __Linux__ kernel, __Security Modules__ (LSM), such as **Hypnos**, are in charge of this decision.

### How does Hypnos work ?

Hypnos stores a set of __rules__ which tells if it should accept or refuse a certain system call.

These rules are special in that it is a **program**, containing instructions, which determines the rules.
The program runs in a virtual machine which supports functions, branching and a set of built-in classes and methods.
The term "rule" is not used internally, as the concept is closer to "program".

### Interacting with Hypnos

#### Trusted process

When a trusted process forks, the new process is trusted too.
The init process (pid 1) is trusted by default, and so are all of its children.

Hypnos does not interfere with system calls from __trusted processes__.

#### Monitored process

Trusted processes can [become __monitored__](#from-trusted-to-monitored).
Monitored processes have an associated application ID.
Rules can set this ID as an acceptance (or denial) criteria.

When a monitored process makes a system call, the stored rules are searched for a match based on the app, the system call, and its arguments. If such a match is found, the rule determines if the system call will be accepted.  
If no match is found, the process has "committed an infringement":
* the system call and its arguments get stored as a string in kernel memory
* an *infringement file* is created (see [the controller interface](#controller-interface))
* Hypnos simulates a SIGTRAP from the monitored process, that a [`ptracer`](https://en.wikipedia.org/wiki/Ptrace) can catch.

#### Controller interface

Hypnos provides an interface for controllers through a `securityfs` directory: `/sys/kernel/security/hypnos`.
* `/update`: used to update the rules used by Hypnos on system calls
* `/register`: see [From trusted to monitored](#from-trusted-to-monitored)
* `/x/y` (numbers): path to an infringement, where `x` is an application ID, and `y` an infringement ID.

### From trusted to monitored

This transition is internally called "registration".  
Trusted processes have access to the `/register` [interface](controller-interface).
By writing an application ID to this file, they signal the kernel that they should now be monitored.
If the `write()` succeeds, it means the process is now monitored.
If it forks, the new process will also be monitored under this application ID.

### Updating the rules

Hypnos rules can be updated by writing data to `/update`.
[This diagram shows the format of the written data.](update_format.png)
The instruction set of the program has not been documented yet.

### Comparison with other security software

#### AppArmor

Both implemented as LSMs, [AppArmor](https://gitlab.com/apparmor/apparmor/) and Hypnos share the same goal.
With AppArmor, resource-access is filtered based on __policy files__.
AppArmor's policies cannot be upgraded without restarting the sandboxed application. This is a problem when, for instance, it took hours to compute something but the result cannot be stored because the application doesn't have that permission. If the policies could be updated seemlessly, as with Hypnos, the problem wouldn't exist.
AppArmor is also much more complex than Hypnos, while allowing the same extent of customization.

#### SecComp

[SecComp](https://en.wikipedia.org/wiki/Seccomp) allows applications to sandbox themselves. It was designed for applications that have a large attack surface, protecting the app's environment in case the app is hijacked. Hypnos, on the other hand, requires no work from the application.

#### SELinux

[SELinux](https://selinuxproject.org/page/Main_Page) is also an LSM. It extends the basic Linux security model with resource labels: matching labels on resource and actor grants the actor's access to the resource. It secures the whole system, it cannot work only on a subset of processes and files, while Hypnos can.

#### Android Permissions

[Android permissions](https://developer.android.com/guide/topics/permissions/overview) directly inspired Hypnos design.
In recent Android versions, an application initally has almost no access to the device's resource. When it tries to access storage, for instance, the user is prompted whether or not to allow the access. Hypnos allows to develop such a system, but also allows for finer-grained permissions: in the previous case, it would allow agents to grant storage access, but only for reading for instance.