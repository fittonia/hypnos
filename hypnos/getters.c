// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * Hypnos / getters of VM classes
 *
 */

#include "include/program.h"
#include <linux/kernel.h>
#include <linux/fs.h>


hypnos_obj_t hlsm_file_get_path(hypnos_obj_t obj){
	struct file *file = obj.content.as_voidp;
	obj.content.as_voidp = &file->f_path;
	obj.type = &hypnos_type_path;
	return obj;
}