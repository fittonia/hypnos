// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * Hypnos / classes + constants + VM functions
 *
 */

#include <linux/module.h>
#include "include/program.h"
#include "include/getters.h"
#include "include/methods.h"
#include "include/to_string.h"


hypnos_type_t hypnos_type_result = { NULL, NULL, 0, 0, {} };
hypnos_type_t hypnos_type_error  = { NULL, NULL, 0, 0, {} };
hypnos_type_t hypnos_type_string = { NULL, NULL, 0, 0, {} };

hypnos_type_t hypnos_type_int  = { &int_str_alloc,  &int_to_string,  0, 2, { {.m=&hlsm_int_if}, {.m=&hlsm_int_and} } };
hypnos_type_t hypnos_type_file = { &file_str_alloc, &file_to_string, 1, 1, { {.g=&hlsm_file_get_path} } };
hypnos_type_t hypnos_type_path = { &path_str_alloc, &path_to_string, 0, 2, { {.m=&hlsm_path_under}, {.m=&hlsm_path_equal} } };


const hypnos_obj_t hypnos_undef = { { .as_voidp = NULL }, NULL };

const hypnos_obj_t hypnos_true  = { { .as_int = 1 }, &hypnos_type_int };
const hypnos_obj_t hypnos_false = { { .as_int = 0 }, &hypnos_type_int };

const hypnos_obj_t hypnos_refuse = { { .as_int = 1 }, &hypnos_type_result };
const hypnos_obj_t hypnos_accept = { { .as_int = 0 }, &hypnos_type_result };

const hypnos_obj_t hypnos_no_such_object = { { .as_charp = "no such object" }, &hypnos_type_error };
const hypnos_obj_t hypnos_no_such_method = { { .as_charp = "no such method" }, &hypnos_type_error };
const hypnos_obj_t hypnos_unexpected_end = { { .as_charp = "reached end of section unexpectedly" }, &hypnos_type_error };
const hypnos_obj_t hypnos_invalid_addr   = { { .as_charp = "invalid address" }, &hypnos_type_error };



hypnos_obj_t hypnos_run(hypnos_scope_t scope, hypnos_program_t program, u16 entry, const char *hook){
	hypnos_obj_t obj;
	if (program.code_size){
		obj = hypnos_call(scope, program, entry);
		if (obj.type == &hypnos_type_error){
			pr_info("Hypnos: filter program for %s exited with error (%s)", hook, obj.content.as_charp);
		}
	} else obj = hypnos_undef;
	return obj;
}

void *hypnos_get_arg(hypnos_program_t program, u16 *i){
	u16 address;
	s8 byte;
	HYPNOS_NEXT_BYTE_NR(*i, program, byte) else return NULL;
	address = byte;
	address <<= 8;
	HYPNOS_NEXT_BYTE_NR(*i, program, byte) else return NULL;
	address |= byte;
	return address < program.data_size ? program.data + address : NULL;
}

hypnos_obj_t hypnos_call_inst(hypnos_scope_t scope, hypnos_program_t program, u16 *i){
	u16 address;
	hypnos_scope_t fn_scope;
	unsigned j;
	s8 byte;

	HYPNOS_NEXT_BYTE(*i, program, byte);
	address = byte;
	address <<= 8;
	HYPNOS_NEXT_BYTE(*i, program, byte);
	address |= byte;
	if (address > program.code_size) return hypnos_invalid_addr;

	for (j = 0; j < 6; j++){
		HYPNOS_NEXT_BYTE(*i, program, byte);
		if (byte >= 6 || byte < 0) fn_scope[j] = hypnos_undef;
		else fn_scope[j] = scope[byte];
	}

	return hypnos_call(fn_scope, program, address);
}

hypnos_obj_t hypnos_call(hypnos_scope_t scope, hypnos_program_t program, u16 i){
	s8 byte;
	hypnos_obj_t obj = hypnos_undef;
	do {

		HYPNOS_NEXT_BYTE(i, program, byte);
		if (byte < 0){

			switch (byte){

				case HYPNOS_CALL:
					obj = hypnos_call_inst(scope, program, &i);
					break;

				case HYPNOS_RET:
					return obj;

				case HYPNOS_ACCEPT:
					return hypnos_accept;

				case HYPNOS_REFUSE:
					return hypnos_refuse;

				default:
					obj = hypnos_no_such_method;

			}

		} else {
			u8 obj_idx = byte & 0b111;
			u8  fn_idx = byte >> 3;

			if (obj_idx < 0b111) obj = scope[obj_idx];
			// else use last return value as obj (nothing to do)

			// CHECK VALID OBJECT
			if (obj.type == NULL) return hypnos_no_such_object;

			// CHECK VALID METHOD
			if (fn_idx < obj.type->callables_len){

				hypnos_callable_t callable = obj.type->callables[fn_idx];

				if (fn_idx < obj.type->getters_len){

					obj = callable.g(obj);
					i += 2;

				} else {

					obj = callable.m(obj, program, &i);

				}

			} else {

				return hypnos_no_such_method;

			}
		}

	} while (obj.type != &hypnos_type_error);

	return obj;
}