// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * Hypnos / methods of VM classes
 *
 */

#include "include/program.h"
#include <linux/kernel.h>
#include <linux/path.h>
#include <linux/namei.h>


hypnos_obj_t hlsm_path_under(hypnos_obj_t obj, hypnos_program_t program, u16 *i){
	struct path *path = obj.content.as_voidp;
	struct path arg_path;
	u_long req_ino;
	struct dentry *parent_dentry;
	char *str_parent = hypnos_get_arg(program, i);

	if (!str_parent) return hypnos_unexpected_end;
	if (kern_path(str_parent, LOOKUP_FOLLOW, &arg_path) >= 0){
		req_ino = arg_path.dentry->d_inode->i_ino;
		parent_dentry = path->dentry->d_parent;
		if (S_ISDIR(arg_path.dentry->d_inode->i_mode)){
			while (parent_dentry != NULL){
				if (parent_dentry->d_inode->i_ino == req_ino){
					return hypnos_true;
				} else parent_dentry = parent_dentry->d_parent;
			}
		}
	}
	return hypnos_false;
}

hypnos_obj_t hlsm_path_equal(hypnos_obj_t obj, hypnos_program_t program, u16 *i){
	struct path *path = obj.content.as_voidp;
	struct path arg_path;
	hypnos_obj_t retval = hypnos_false;
	char *str_arg_path = hypnos_get_arg(program, i);

	if (!str_arg_path) return hypnos_unexpected_end;
	if (kern_path(str_arg_path, LOOKUP_FOLLOW, &arg_path) >= 0){
		u_long req_ino = arg_path.dentry->d_inode->i_ino;
		retval.content.as_int = path->dentry->d_inode->i_ino == req_ino;
	}

	return retval;
}

hypnos_obj_t hlsm_int_if(hypnos_obj_t obj, hypnos_program_t program, u16 *i){
	int *data = hypnos_get_arg(program, i);
	if (!obj.content.as_int) *i += *data;
	return hypnos_undef;
}

hypnos_obj_t hlsm_int_and(hypnos_obj_t obj, hypnos_program_t program, u16 *i){
	int *b = hypnos_get_arg(program, i);
	obj.content.as_int &= *b;
	return obj;
}
