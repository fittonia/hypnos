/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef HYPNOS_UPDATE_H
#define HYPNOS_UPDATE_H

#include "program.h"

#include <linux/types.h>
#include <linux/fs.h>
#include <stddef.h>

extern struct mutex hypnos_upd;
extern bool hypnos_updating;

extern ssize_t hypnos_update(struct file * file, const char __user *userbuf, size_t len, loff_t *ppos);

#endif