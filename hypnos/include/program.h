/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef HYPNOS_PROGRAM_H
#define HYPNOS_PROGRAM_H

#include <stddef.h>
#include <linux/types.h>

#define HYPNOS_CALL   -1
#define HYPNOS_RET    -2
#define HYPNOS_ACCEPT -3
#define HYPNOS_REFUSE -4

typedef struct hypnos_obj hypnos_obj_t;
typedef struct hypnos_program hypnos_program_t;

typedef size_t (*hypnos_str_alloc_t)(hypnos_obj_t);
typedef size_t (*hypnos_to_string_t)(hypnos_obj_t, char *);

typedef hypnos_obj_t (*hypnos_getter_t)(hypnos_obj_t);
typedef hypnos_obj_t (*hypnos_method_t)(hypnos_obj_t, hypnos_program_t, u16 *);

typedef union {
	hypnos_getter_t g;
	hypnos_method_t m;
} hypnos_callable_t;

typedef struct hypnos_program {
	s8 *data;
	s8 *code;
	u16 data_size;
	u16 code_size;
} hypnos_program_t;

typedef struct hypnos_type {
	hypnos_str_alloc_t str_alloc;
	hypnos_to_string_t to_string;
	const size_t getters_len;
	const size_t callables_len;
	const hypnos_callable_t callables[];
} hypnos_type_t;

typedef struct hypnos_obj {
	union {
		int as_int;
		void *as_voidp;
		char *as_charp;
	} content;
	hypnos_type_t *type;
} hypnos_obj_t;

typedef hypnos_obj_t hypnos_scope_t[6];


extern hypnos_type_t hypnos_type_int;
extern hypnos_type_t hypnos_type_result;
extern hypnos_type_t hypnos_type_error;
extern hypnos_type_t hypnos_type_file;
extern hypnos_type_t hypnos_type_path;
extern hypnos_type_t hypnos_type_string;

const extern hypnos_obj_t hypnos_undef;

const extern hypnos_obj_t hypnos_true;
const extern hypnos_obj_t hypnos_false;

const extern hypnos_obj_t hypnos_refuse;
const extern hypnos_obj_t hypnos_accept;

const extern hypnos_obj_t hypnos_no_such_object;
const extern hypnos_obj_t hypnos_no_such_method;
const extern hypnos_obj_t hypnos_unexpected_end;
const extern hypnos_obj_t hypnos_invalid_addr;



#define HYPNOS_NEXT_BYTE_NR(i, p, byte) if ((i) < (p).code_size){ (byte) = (p).code[(i)++]; }
#define HYPNOS_NEXT_BYTE(i, p, byte) HYPNOS_NEXT_BYTE_NR((i), (p), (byte)) else return hypnos_unexpected_end;

void *hypnos_get_arg(hypnos_program_t program, u16 *i);

hypnos_obj_t hypnos_run(hypnos_scope_t scope, hypnos_program_t program, u16 entry, const char *hook);

hypnos_obj_t hypnos_call(hypnos_scope_t scope, hypnos_program_t program, u16 i);

#endif