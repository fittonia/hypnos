/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef HYPNOS_METHODS_H
#define HYPNOS_METHODS_H

extern hypnos_obj_t hlsm_path_under(hypnos_obj_t obj, hypnos_program_t program, u16 *i);

extern hypnos_obj_t hlsm_path_equal(hypnos_obj_t obj, hypnos_program_t program, u16 *i);

extern hypnos_obj_t hlsm_int_if(hypnos_obj_t obj, hypnos_program_t program, u16 *i);

extern hypnos_obj_t hlsm_int_and(hypnos_obj_t obj, hypnos_program_t program, u16 *i);

#endif