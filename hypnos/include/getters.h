/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef HYPNOS_GETTERS_H
#define HYPNOS_GETTERS_H

extern hypnos_obj_t hlsm_file_get_path(hypnos_obj_t obj);

#endif