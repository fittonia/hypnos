/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef HYPNOS_TO_STRING_H
#define HYPNOS_TO_STRING_H

// INT
extern size_t int_str_alloc(hypnos_obj_t obj);
extern size_t int_to_string(hypnos_obj_t obj, char *output);

// FILE
extern size_t file_str_alloc(hypnos_obj_t obj);
extern size_t file_to_string(hypnos_obj_t obj, char *output);

// PATH
extern size_t path_str_alloc(hypnos_obj_t obj);
extern size_t path_to_string(hypnos_obj_t obj, char *output);

#endif