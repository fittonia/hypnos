/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef HYPNOS_LSM_H
#define HYPNOS_LSM_H

#include <linux/module.h>
#include <linux/kernel.h>

// there has to be a better way

#define WAIT_FOR_ZERO(mutex, var) \
	do {                          \
		bool r;                   \
		mutex_lock(mutex);        \
		r = (var) == 0;           \
		mutex_unlock(mutex);      \
		if (r) break;             \
		msleep(100);              \
	} while (true);
//

#define MUTEXD_OP(mutex, opration) \
	mutex_lock(mutex);             \
	opration;                      \
	mutex_unlock(mutex);           \
//

typedef unsigned app_t;
#define HYPNOS_TRUSTED_APP ((unsigned)~0U)

typedef struct {
	app_t app;
	struct dentry *dir;
} hypnos_task_blob_t;

typedef enum { ACCEPT, REFUSE, RERUN } hypnos_prompt_t;

typedef struct {
	// infringement info
	char *summary;
	size_t length;
	hypnos_prompt_t result;
} hypnos_inode_blob_t;

#define HYPNOS_BLOB(task) ((hypnos_task_blob_t *)((task)->security + hypnos_blob_sizes.lbs_task))
#define HYPNOS_APP(task) (&(HYPNOS_BLOB(task))->app)
#define HYPNOS_DIR(task) (&(HYPNOS_BLOB(task))->dir)

extern struct mutex hypnos_rh_me;
extern size_t hypnos_running_hooks;

extern struct dentry  *hypnos_create_infringement_file(char *name, char *summary, size_t length);

extern hypnos_prompt_t hypnos_delete_infringement_file(struct dentry *d);

#endif