/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef HYPNOS_HOOKS_H
#define HYPNOS_HOOKS_H

#include "program.h"
#include "lsm.h"

#include <linux/lsm_hooks.h>

#define HYPNOS_HOOK6(num, name, a1, a2, a3, a4, a5, a6)                            \
	int rc = 0;                                                                    \
	unsigned app = hypnos_hook_table ? *HYPNOS_APP(current) : HYPNOS_TRUSTED_APP;  \
	if (app != HYPNOS_TRUSTED_APP){                                                \
		hypnos_scope_t scope = { (a1), (a2), (a3), (a4), (a5), (a6) };             \
		rc = hypnos_hook(app, num, scope, #name);                                  \
	}                                                                              \
	return rc;                                                                     \
//

#define HYPNOS_HOOK5(num, name, a1, a2, a3, a4, a5) HYPNOS_HOOK6(num, name, (a1), (a2), (a3), (a4), (a5), hypnos_undef)
#define HYPNOS_HOOK4(num, name, a1, a2, a3, a4)     HYPNOS_HOOK5(num, name, (a1), (a2), (a3), (a4), hypnos_undef)
#define HYPNOS_HOOK3(num, name, a1, a2, a3)         HYPNOS_HOOK4(num, name, (a1), (a2), (a3), hypnos_undef)
#define HYPNOS_HOOK2(num, name, a1, a2)             HYPNOS_HOOK3(num, name, (a1), (a2), hypnos_undef)
#define HYPNOS_HOOK1(num, name, a1)                 HYPNOS_HOOK2(num, name, (a1), hypnos_undef)
#define HYPNOS_HOOK0(num, name)                     HYPNOS_HOOK1(num, name, hypnos_undef)

#define HYPNOS_OBJ(__type, val) ((hypnos_obj_t){.type=&(hypnos_type_##__type),.content={.as_##__type=mask}})

extern struct lsm_blob_sizes hypnos_blob_sizes; // in lsm.c

extern u16 hypnos_apps;

extern const u16 hypnos_hooks;
extern struct security_hook_list hypnos_lsm_hooks[];

extern hypnos_program_t hypnos_program;
extern u16 *hypnos_hook_table;

extern int hypnos_hook(unsigned app, unsigned hook, hypnos_scope_t scope, char *name);

#endif