// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * Hypnos / declaration of LSM hooks
 *
 */

#include "include/hooks.h"
#include "include/lsm.h"
#include "include/update.h"

#include <linux/module.h>
#include <linux/sched/signal.h>
#include <linux/kernel.h>
#include <linux/ptrace.h>
#include <linux/delay.h>

hypnos_program_t hypnos_program = { NULL, 0, 0 };
u16 *hypnos_hook_table = NULL;

char *hypnos_parameters(hypnos_scope_t scope, size_t *len_p){
	unsigned i;
	char *string, *retval;
	size_t length;
	size_t alloc_sz = 2;

	for (i = 0; i < 6; i++){
		hypnos_obj_t obj = scope[i];
		if (obj.type == NULL) break;
		alloc_sz += obj.type->str_alloc(obj) + 2;
	}

	retval = kmalloc(alloc_sz, GFP_KERNEL);
	length = 2;
	memcpy(retval, "[ ", length);
	string = retval + 2;

	for (i = 0; i < 6; i++){
		size_t part;
		hypnos_obj_t obj = scope[i];
		if (obj.type == NULL) break;

		part = obj.type->to_string(obj, string);
		string += part;
		memcpy(string, ", ", 2);
		string += 2;
		length += 2 + part;

	}

	string -= 2;
	memcpy(string, " ]", length);

	*len_p = length;
	return retval;
}

int hypnos_hook(unsigned app, unsigned hook, hypnos_scope_t scope, char *name){
	u16 entry;
	hypnos_obj_t obj = hypnos_undef;

	WAIT_FOR_ZERO(&hypnos_upd, hypnos_updating)

	MUTEXD_OP(&hypnos_rh_me, hypnos_running_hooks++);

	entry = hypnos_hook_table[hypnos_hooks * app + hook];
	if (entry != U16_MAX){
		obj = hypnos_run(scope, hypnos_program, entry, name);
	}

	MUTEXD_OP(&hypnos_rh_me, hypnos_running_hooks--);

	if (obj.type != &hypnos_type_result){
		int retval = 1;
		size_t length;
		char *summary = hypnos_parameters(scope, &length);
		
		struct dentry *d = hypnos_create_infringement_file(name, summary, length);
		if (d){
			hypnos_prompt_t p;
			ptrace_notify(SIGTRAP);
			p = hypnos_delete_infringement_file(d);
			if (p == ACCEPT) retval = 0;
		}

		kfree(summary);
		return retval;
	} else return obj.content.as_int;
}


static int hypnos_task_alloc(struct task_struct *task, unsigned long clone_flags){
	*HYPNOS_BLOB(task) = *HYPNOS_BLOB(current);
	return 0;
}

static int hypnos_path_rmdir(const struct path *dir, struct dentry *dentry){
	HYPNOS_HOOK2(0, path_rmdir, hypnos_false, hypnos_false)
}

static int hypnos_path_mkdir(const struct path *dir, struct dentry *dentry, umode_t mode){
	HYPNOS_HOOK3(1, path_mkdir, hypnos_false, hypnos_false, hypnos_false)
}

static int hypnos_file_permission(struct file *file, int mask){
	hypnos_obj_t file_obj = { .type = &hypnos_type_file, .content = { .as_voidp = file } };
	HYPNOS_HOOK2(2, file_perm, file_obj, HYPNOS_OBJ(int, mask))
}

struct security_hook_list hypnos_lsm_hooks[] __lsm_ro_after_init = {
	LSM_HOOK_INIT(path_rmdir, hypnos_path_rmdir),
	LSM_HOOK_INIT(path_mkdir, hypnos_path_mkdir),
	LSM_HOOK_INIT(file_permission, hypnos_file_permission),
	LSM_HOOK_INIT(task_alloc, hypnos_task_alloc),
};

u16 hypnos_apps = 0;
const u16 hypnos_hooks = ARRAY_SIZE(hypnos_lsm_hooks);