// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * Hypnos / converters to string for VM classes
 * Used by hooks.c:hypnos_parameters
 *
 */

#include "include/program.h"
#include "include/getters.h"
#include "include/to_string.h"
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/path.h>
#include <linux/namei.h>
#include <linux/err.h>

// INT

size_t int_str_alloc(hypnos_obj_t obj){
	return 12;
}

size_t int_to_string(hypnos_obj_t obj, char *output){
	return sprintf(output, "%d", obj.content.as_int);
}

// FILE

size_t file_str_alloc(hypnos_obj_t obj){
	hypnos_obj_t path = hlsm_file_get_path(obj);
	return 2 + path_str_alloc(path) + 2;
}

size_t file_to_string(hypnos_obj_t obj, char *output){
	size_t path_len;
	hypnos_obj_t path = hlsm_file_get_path(obj);
	memcpy(output, "{ ", 2);
	path_len = path_to_string(path, output + 2);
	memcpy(output + 2 + path_len, " }", 2);
	return 2 + 2 + path_len;
}

// PATH

size_t path_str_alloc(hypnos_obj_t obj){
	return 4096;
}

size_t path_to_string(hypnos_obj_t obj, char *output){
	size_t len;
	void *orig;
	char *str = orig = kmalloc(4096, GFP_KERNEL);

	str = d_path(obj.content.as_voidp, str, 4096);
	if (IS_ERR(str)) str = "[d_path error]";
	len = strlen(str);
	memcpy(output, str, len);

	kfree(orig);

	return len;
}