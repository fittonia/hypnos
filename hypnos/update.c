// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * Hypnos / filter update
 *
 */

#include "include/update.h"
#include "include/hooks.h"
#include "include/lsm.h"

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/slab.h>

DEFINE_MUTEX(hypnos_upd);
bool hypnos_updating = false;

typedef struct update_header {
	s8 sig[4];
	u32 apps;
	u32 bindings;
	u32 data_size;
	u32 functions;
} __attribute__ ((packed)) update_header_t;

typedef struct hook_binding {
	u16 app;
	u16 hook;
	u32 index;
} __attribute__ ((packed)) hook_binding_t;

typedef struct reader {
	const char __user *userbuf;
	size_t available;
} reader_t;

bool hypnos_apply_update(hypnos_program_t program, u16 *hook_table, u32 apps){

	// prevent concurrent updates
	if (hypnos_updating) return true;

	// stop future hook runs
	MUTEXD_OP(&hypnos_upd, hypnos_updating = true);

	// wait for previously launched hook runs to end
	WAIT_FOR_ZERO(&hypnos_rh_me, hypnos_running_hooks)

	// apply the updates
	kfree(hypnos_program.data);
	kfree(hypnos_hook_table);
	hypnos_program = program;
	hypnos_hook_table = hook_table;
	hypnos_apps = apps;

	// restart hooks
	MUTEXD_OP(&hypnos_upd, hypnos_updating = false);

	return false;

}

bool can_read_from_update(size_t sz, reader_t *reader){
	return sz <= reader->available && access_ok(reader->userbuf, sz);
}

bool read_from_update(void *dst, size_t sz, reader_t *reader){
	bool result = copy_from_user(dst, reader->userbuf, sz) == 0;
	reader->userbuf += sz;
	reader->available -= sz;
	return !result;
}


bool check_update_header(update_header_t *header){
	size_t i;
	const char *sig = "XSF1";

	for (i = 0; i < 4; i++){
		if (sig[i] != header->sig[i]) return true;
	}

	return header->functions != 0;
}

const u8 inst_sizes[] = { 3, 9, 1, 1, 1 };

u16 *build_fn_table(u32 functions, hypnos_program_t program){
	u32 fn_index = 0;
	size_t i;
	u16 entry_point = 0;
	u16 *function_table = kmalloc_array(functions, sizeof(*function_table), GFP_KERNEL);

	for (i = 0; i < program.code_size;){
		s8 inst = program.code[i];

		if (inst < HYPNOS_REFUSE){
			break;

		} else if (inst == HYPNOS_RET){
			// mark the end of a function
			function_table[fn_index++] = entry_point;

		} else if (inst == HYPNOS_CALL){
			u16 *offset_p = (u16 *)(program.code + 1);
			u16 index = *offset_p;
			// can only call already defined functions
			if (index >= fn_index) break;
			// translate function index into program offset
			*offset_p = function_table[index];

		} else if (inst >= 0){
			// -> HYPNOS_METHOD
			u16 data_ofst = *(u16 *)(program.code + 1);
			// does this need more checks ?
			if (data_ofst >= program.data_size) break;
			inst = 0;
		}

		i += inst_sizes[inst * -1];

		if (inst == HYPNOS_RET){
			entry_point = i;
			if (fn_index >= functions) break;
		}
	}

	if (fn_index != functions || i != program.code_size){
		kfree(function_table);
		function_table = NULL;
	}

	return function_table;
}

u16 *build_hook_table(update_header_t header, u16 *function_table, hook_binding_t *bindings){
	size_t i, hook_table_cap = hypnos_hooks * header.apps;
	hook_binding_t *hb = bindings;

	u16 *hook_table = kmalloc_array(hook_table_cap, sizeof(*hook_table), GFP_KERNEL);
	for (i = 0; i < hook_table_cap; i++) hook_table[i] = U16_MAX;

	for (i = 0; i < header.bindings; i++, hb++){
		if (hb->hook < hypnos_hooks && hb->index < header.functions && hb->app < header.apps){
			hook_table[hb->app * hypnos_hooks + hb->hook] = function_table[hb->index];
		} else break;
	}

	if (i != header.bindings){
		kfree(hook_table);
		hook_table = NULL;
	}

	return hook_table;
}

int update_program(reader_t reader){
	update_header_t header;
	hook_binding_t *bindings;
	size_t bindings_sz;
	hypnos_program_t program;
	u16 *function_table, *hook_table;

	// read header
	if (read_from_update(&header, sizeof(header), &reader)) return -ENOMEM;

	// check header
	if (check_update_header(&header)) return -EILSEQ;

	// calc bindings memory
	bindings_sz = header.bindings * sizeof(*bindings);

	// check overflow
	if ((SIZE_MAX - bindings_sz) < header.data_size) return -ENOMEM;

	// check layout coherence
	if (can_read_from_update(bindings_sz + header.data_size, &reader)) return -ENOMEM;

	// alloc bindings
	bindings = kmalloc(bindings_sz, GFP_KERNEL);

	// read bindings
	if (read_from_update(bindings, bindings_sz, &reader)) return -ENOMEM;

	// program dimensions
	program.data_size = header.data_size;
	program.code_size = reader.available - header.data_size;

	// alloc data + code + airbag
	program.data = kmalloc(reader.available + 1, GFP_KERNEL);
	program.code = program.data + program.data_size;
	*(program.code++) = '\0'; // terminating bad strings in data

	// read data
	if (read_from_update(program.data, program.data_size, &reader)) return -ENOMEM;

	// read code
	if (read_from_update(program.code, program.code_size, &reader)) return -ENOMEM;

	// build function table
	function_table = build_fn_table(header.functions, program);
	if (function_table == NULL) return -EILSEQ;

	// build hook table
	hook_table = build_hook_table(header, function_table, bindings);
	if (hook_table == NULL) return -EILSEQ;

	kfree(function_table);

	hypnos_apply_update(program, hook_table, header.apps);

	return 0;
}

ssize_t hypnos_update(struct file * file, const char __user *userbuf, size_t len, loff_t *ppos){
	int ret = (*ppos != 0) ? -EINVAL : update_program((reader_t){ userbuf, len });
	return ret ? ret : len;
}