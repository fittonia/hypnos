// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * Hypnos / SecurityFS dentries + LSM declaration
 *
 */

#include "include/hooks.h"
#include "include/update.h"
#include "include/lsm.h"

#include <linux/sched/signal.h>
#include <linux/lsm_hooks.h>
#include <linux/security.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/namei.h>

DEFINE_MUTEX(hypnos_rh_me);
size_t hypnos_running_hooks = 0;

static struct dentry *hypnos_dir;





static int hypnos_reg_set(unsigned app){
	unsigned *app_p = HYPNOS_APP(current);
	struct dentry **dir = HYPNOS_DIR(current);

	if (*app_p == HYPNOS_TRUSTED_APP && app < hypnos_apps){
		char dir_name[11];
		struct dentry *d;
		int len;

		len = sprintf(dir_name, "%u", app);
		d = securityfs_create_dir(dir_name, hypnos_dir);

		if (IS_ERR(d)){
			struct inode *hypnos_ino = d_inode(hypnos_dir);
			inode_lock(hypnos_ino);
			d = lookup_one_len(dir_name, hypnos_dir, len);
			inode_unlock(hypnos_ino);
		}

		if (!IS_ERR(d)){
			*dir = d;
			*app_p = app;
			return 0;
		}
	}

	return -1;
}

static unsigned hypnos_reg_get(void){
	return *HYPNOS_APP(current);
}

static ssize_t hypnos_register_write(struct file *file, const char __user *userbuf, size_t len, loff_t *ppos){
	unsigned val;
	int err;
	char *val_str;
	if (*ppos != 0) return 0;

	val_str = kmalloc(len + 1, GFP_KERNEL);
	if (copy_from_user(val_str, userbuf, len)){
		return -1;
	}
	val_str[len] = '\0';

	err = kstrtouint(val_str, 10, &val);
	kfree(val_str);

	if (!err) err = hypnos_reg_set(val);

	return err ? err : len;
}

static ssize_t hypnos_register_read(struct file *file, char __user *userbuf, size_t len, loff_t *ppos){
	unsigned val;
	int val_str_len;
	char val_str[30];

	if (*ppos != 0) return 0;

	val = hypnos_reg_get();
	val_str_len = snprintf(val_str, 30, "%u", hypnos_reg_get());
	if (val_str_len >= len){
		return -1;
	}

	if (copy_to_user(userbuf, val_str, val_str_len)){
		return -EFAULT;
	}

	return val_str_len;
}

static const struct file_operations hypnos_reg_fops = {
	.write = hypnos_register_write,
	.read = hypnos_register_read,
};

static const struct file_operations hypnos_upd_fops = {
	.write = hypnos_update, // update.h
};

static ssize_t hypnos_infringement_write(struct file *file, const char __user *userbuf, size_t len, loff_t *ppos){
	char c;
	hypnos_inode_blob_t *blob = file->f_inode->i_security;

	if (copy_from_user(&c, userbuf, 1)){
		return -1;
	}

	/**/ if (c == 'a') blob->result = ACCEPT;
	else if (c == 'r') blob->result = REFUSE;
	else if (c == 'u') blob->result = RERUN;

	return len;
}

static ssize_t hypnos_infringement_read(struct file *file, char __user *userbuf, size_t len, loff_t *ppos){
	hypnos_inode_blob_t *blob = file->f_inode->i_security;

	if (copy_to_user(userbuf, blob->summary, blob->length)){
		return -EFAULT;
	}

	return blob->length;
}

const struct file_operations hypnos_inf_fops = {
	.write = hypnos_infringement_write,
	.read = hypnos_infringement_read,
};





// called by registered racks ONLY

struct dentry *hypnos_create_infringement_file(char *name, char *summary, size_t length){
	struct dentry *d = NULL, *dir = *HYPNOS_DIR(current);
	if (dir){
		hypnos_inode_blob_t *blob;
		d = securityfs_create_file(name, 0666, dir, NULL, &hypnos_inf_fops);
		if (!IS_ERR(d)){
			blob = d->d_inode->i_security;
			blob->summary = summary;
			blob->length = length;
			blob->result = REFUSE;
		} else d = NULL;
	}
	return d;
}

hypnos_prompt_t hypnos_delete_infringement_file(struct dentry *d){
	hypnos_prompt_t p;
	hypnos_inode_blob_t *blob = d->d_inode->i_security;
	p = blob->result;
	securityfs_remove(d);
	return p;
}





static int hypnos_init_fs(void){
	hypnos_dir = securityfs_create_dir("hypnos", NULL);
	if (hypnos_dir){
		securityfs_create_file("update",   0666, hypnos_dir, NULL, &hypnos_upd_fops);
		securityfs_create_file("register", 0666, hypnos_dir, NULL, &hypnos_reg_fops);
	}

	return 0;
}
__initcall(hypnos_init_fs);

static int __init hypnos_init(void){
	// assuming that all other tasks will be descendants of init_task

	// current = &init_task
	*HYPNOS_APP(current) = HYPNOS_TRUSTED_APP;

	security_add_hooks(hypnos_lsm_hooks, hypnos_hooks, "hypnos");

	pr_info("Hypnos: Initialized (%u hooks)\n", hypnos_hooks);
	return 0;
}

struct lsm_blob_sizes hypnos_blob_sizes __lsm_ro_after_init = {
	.lbs_task = sizeof(hypnos_task_blob_t),
	.lbs_inode = sizeof(hypnos_inode_blob_t),
};

DEFINE_LSM(hypnos) = {
	.name = "hypnos",
	.init = hypnos_init,
	.blobs = &hypnos_blob_sizes,
};
